import styled from 'styled-components/native'

const Text = styled.Text`
  color: ${({ theme }) => theme.primaryTextColor};
  font-size: ${({ fontSize }) => (fontSize || 24)};
`
export default Text
