import * as React from 'react'
import Icon from 'react-native-vector-icons/Feather'

import HeaderButton from './HeaderButton'

const ExitButton = ({ onPress }) => (
  <HeaderButton onPress={onPress}>
    <Icon name='x' color='#fff' size={30} />
  </HeaderButton>
)
export default ExitButton
