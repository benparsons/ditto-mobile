import React from 'react'
import styled from 'styled-components/native'

type TextButtonProps = {
  children: any,
  onPress: Function,
  color?: string,
  alignCenter?: boolean,
  size?: 'sm' | 'md' | 'lg'
};

const TouchableWrapper = styled.TouchableOpacity`
  padding: 10px 20px;
`

const LinkText = styled.Text`
  color: ${({ color, theme }) => color || theme.primaryTextColor};
  text-decoration: underline;
  text-decoration-color: ${({ color, theme }) => color || theme.primaryTextColor};
  font-weight: bold;
  font-size: ${({ size }) => {
      switch (size) {
        case 'sm':
          return 14
        case 'md':
          return 18
        case 'lg':
          return 24
        default:
          return 16
      }
    }}
    ${({ alignCenter }) => (alignCenter ? 'text-align: center;' : '')};
`

const TextButton = ({
  children,
  onPress = () => {},
  color = null,
  alignCenter = false,
  size = 'sm'
}: TextButtonProps) => (
  <TouchableWrapper onPress={onPress}>
    <LinkText color={color} alignCenter={alignCenter} size={size}>
      {children}
    </LinkText>
  </TouchableWrapper>
)
export default TextButton
