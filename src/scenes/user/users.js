import { Q } from '@nozbe/watermelondb'

import matrix from '../../services/matrix/service'
import auth from '../auth/auth'
import userService from './service'

// const debug = require('debug')('ditto:scenes:user:users')

class Users {
  getAvatarUrl (url) {
    return matrix.getImageUrl(url, 150, 150, 'crop')
  }

  getCount () {
    return userService.getUsers().query().observeCount()
  }

  getKnownUsers () {
    // TODO: Order by popularity?
    return userService.getUsers().query(Q.where('id', Q.notEq(auth.getUserId()))).observe()
  }

  getUser (userId) {
    return userService.getUsers().findAndObserve(userId)
  }

  async getUsers (userIds) {
    return userService.getUsers().query(Q.where('id', Q.oneOf(userIds))).fetch()
  }

  async searchUsers (searchText) {
    const { results: userList } = await matrix.searchUsers(searchText)
    const cleanUserList = []

    // We need to remove duplicates and our own user
    for (const user of userList) {
      if (user.user_id !== auth.getUserId() &&
          cleanUserList.findIndex(cleanUser => cleanUser.id === user.user_id) === -1) {
        cleanUserList.push({
          id: user.user_id,
          name: user.display_name,
          avatar: user.avatar_url
        })
      }
    }
    return cleanUserList
  }

  setAvatar (image) {
    userService.setAvatar(image)
  }

  setName (name) {
    userService.setName(name)
  }
}

const users = new Users()
export default users
