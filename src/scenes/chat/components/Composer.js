import { useTheme } from '@ui-kitten/components'
import React, { useEffect, useState } from 'react'
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import styled from 'styled-components/native'

import ChevronsUp from '../../../assets/icons/icon-chevrons-up.svg'
import IconPlus from '../../../assets/icons/icon-plus.svg'
import { COLORS } from '../../../constants'
import { isIos } from '../../../utilities/Misc'
import chats from '../chats'

export default function Composer ({ chat, onSend, onImagePick }) {
  const [messageValue, setMessageValue] = useState('')
  const theme = useTheme()

  const handleSend = () => {
    onSend(messageValue)
    setMessageValue('')
  }

  useEffect(() => {
    const isTyping = messageValue.length > 0
    chats.isTyping(chat.id, isTyping)
  }, [chat.id, messageValue])

  return (
    <Wrapper>
      <ActionButton style={{ padding: 10, backgroundColor: theme['color-primary-active'] }} onPress={onImagePick}>
        <IconPlus fill={theme['text-basic-color']} width={18} height={18} />
      </ActionButton>
      <GrowingTextInput
        placeholder='Type a message...'
        placeholderTextColor='rgba(255,255,255,.3)'
        value={messageValue}
        onChangeText={setMessageValue}
        style={{ borderColor: theme['border-basic-color-1'] }}
      />
      <ActionButton
        disabled={messageValue.length === 0}
        style={{ padding: isIos() ? 10 : 0, backgroundColor: theme['color-primary-active'] }}
        onPress={handleSend}
      >
        {isIos() ? (
          <ChevronsUp fill={theme['text-basic-color']} width={16} height={16} />
        ) : (
          <Icon name='send' size={22} color='#fff' />
        )}
      </ActionButton>
    </Wrapper>
  )
}

const Wrapper = styled.SafeAreaView`
  flex-direction: row;
  margin-top: 10;
  margin-bottom: 10;
`

const verticalPadding = isIos() ? 7 : 4
const GrowingTextInput = styled(AutoGrowingTextInput)`
  flex: 1;
  border-radius: 20;
  padding-top: ${verticalPadding};
  padding-bottom: ${verticalPadding};
  padding-left: 14;
  padding-right: 14;
  color: ${COLORS.gray.one};
  font-size: 16;
  letter-spacing: 0.3;
  font-weight: 400;
  height: ${isIos() ? 32 : 35};
  border-width: 1;
`

const actionButtonSize = isIos() ? 32 : 35
const ActionButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  border-radius: 40;
  margin-left: 8;
  margin-right: 8;
  width: ${actionButtonSize};
  height: ${actionButtonSize};
  align-self: flex-end;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
`
