import React from 'react'

import EventMessage from './components/EventMessage'
import ImageMessage from './components/ImageMessage'
import NoticeMessage from './components/NoticeMessage'
import TextMessage from './components/TextMessage'
import TypingIndicator from './components/TypingIndicator'

// const debug = require('debug')('ditto:scenes:chat:message:Message')

export default function Message ({ message, prevSender, nextSender, ...otherProps }) {
  switch (message.type) {
    case 'text':
      return (
        <TextMessage
          message={message}
          prevSender={prevSender}
          nextSender={nextSender}
          {...otherProps}
        />
      )
    case 'image':
      return (
        <ImageMessage
          message={message}
          prevSender={prevSender}
          nextSender={nextSender}
          {...otherProps}
        />
      )
    case 'notice':
      return (
        <NoticeMessage
          message={message}
          prevSender={prevSender}
          nextSender={nextSender}
          {...otherProps}
        />
      )
    case 'typing':
      return (
        <TypingIndicator />
      )
    case undefined:
    case '':
      return null
    default:
      return (
        <EventMessage
          message={message}
          {...otherProps}
        />
      )
  }
}
