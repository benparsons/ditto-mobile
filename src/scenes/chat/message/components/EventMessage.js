import withObservables from '@nozbe/with-observables'
import React from 'react'
import styled from 'styled-components/native'

import messages from '../messages'

// const debug = require('debug')('ditto:scenes:chat:message:components:EventMessage')

function EventMessage ({ message, sender }) {
  return (
    <StyledText>{messages.getContent(message, sender)}</StyledText>
  )
}
const enhance = withObservables(['message'], ({ message }) => ({
  sender: message.sender
}))
export default enhance(EventMessage)

const StyledText = styled.Text`
  color: rgba(255, 255, 255, 0.3);
  font-size: 12;
  align-self: center;
  margin-top: 8;
  margin-bottom: 4;
  margin-left: 24;
  margin-right: 24;
  text-align: center;
`
