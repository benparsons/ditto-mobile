import withObservables from '@nozbe/with-observables'
import React from 'react'
import Image from 'react-native-scalable-image'
import styled from 'styled-components/native'

import { getNameColor } from '../../../../utilities/Misc'
import auth from '../../../auth/auth'
import messages from '../messages'

// const debug = require('debug')('ditto:scene:chat:message:components:ImageMessage')

const PlaceholderImage = require('../../../../assets/images/placeholder.png')

function ImageMessage ({ message, sender, prevSender, nextSender }) {
  const userId = auth.getUserId()
  const imageUrl = messages.getContent(message, sender, 'thumb')

  const prevSenderSame = prevSender === sender.id
  const nextSenderSame = nextSender === sender.id

  return (
    <>
      <ImageWrapper
        myMessage={userId === sender.id}
        prevSenderSame={prevSenderSame}
        nextSenderSame={nextSenderSame}
      >
        <Image
          source={{ uri: imageUrl }}
          width={250}
          style={{ borderRadius: 20, minWidth: 250, minHeight: 180 }}
          defaultSource={PlaceholderImage}
        />
      </ImageWrapper>
      {!prevSenderSame && (
        <SenderText
          isUser={userId === sender.id}
          color={getNameColor(sender.id)}
        >
          {sender.name}
        </SenderText>
      )}
    </>
  )
}
const enhance = withObservables(['message'], ({ message }) => ({
  sender: message.sender
}))
export default enhance(ImageMessage)

const ImageWrapper = styled.View`
  align-self: ${({ myMessage }) => (myMessage ? 'flex-end' : 'flex-start')};
  margin-left: 12;
  margin-right: 12;
  margin-top: 2;
  margin-bottom: ${({ nextSenderSame }) => (nextSenderSame ? 1 : 4)};
`

const SenderText = styled.Text`
  color: ${({ color }) => (color || 'pink')};
  font-size: 14;
  font-weight: 400;
  margin-left: 22;
  margin-right: 22;
  margin-top: 8;
  opacity: 0.8;
  ${({ isUser }) => (isUser ? 'text-align: right;' : '')};
`
