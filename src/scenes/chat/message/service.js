import { Q } from '@nozbe/watermelondb'
import _ from 'lodash'
import { BehaviorSubject, Subject } from 'rxjs'

import matrix from '../../../services/matrix/service'
import database from '../../../services/storage/database'

const debug = require('debug')('ditto:scenes:messages:service')

// TODO: clean text message content by removing markdown, edit mark, etc…
// TODO: handle removed latestMessage

class MessageService {
  _messages
  _latestMessages
  _nextLatestMessages
  _syncList
  _nextSyncList
  _isSyncing
  _action
  actions

  constructor () {
    this._messages = database.getCollection('messages')
    this._latestMessages = {}
    this._nextLatestMessages = {}
    this._syncList = {}
    this._nextSyncList = {}
    this._isSyncing = false
    this._action = new Subject(null)
    this.actions = {
      MESSAGE_SEND: 'message/send'
    }
  }

  async init () {
    matrix.getAction().subscribe((action) => {
      switch (action.type) {
        case matrix.actions.EVENT_ROOM_LOCAL:
          this._handleLocalEvent(action.payload.event, action.payload.room, action.payload.oldEventId)
          break
        case matrix.actions.EVENT_ROOM_TIMELINE:
          this._handleTimelineEvent(action.payload.event, action.payload.room)
          break
        default:
      }
    })

    this.getAction().subscribe((action) => {
      switch (action.type) {
        case this.actions.MESSAGE_SEND:
          matrix.setAction({ type: matrix.actions.MESSAGE_SEND, payload: action.payload })
          break
        default:
      }
    })

    const latestMessages = await this._messages.unsafeFetchRecordsWithSQL('SELECT *, MAX(sent_at) FROM messages GROUP BY room_id')
    for (const latestMessage of latestMessages) {
      this._latestMessages[latestMessage.room.id] = new BehaviorSubject(latestMessage)
    }
  }

  getAction () {
    return this._action
  }

  setAction (action) {
    if (!Object.values(this.actions).includes(action.type)) throw Error('Unknown action type:', action)
    debug('Set action:', action)
    this._action.next(action)
  }

  getLatestMessage (roomId) {
    if (!this._latestMessages[roomId]) this._latestMessages[roomId] = new BehaviorSubject(null)
    return this._latestMessages[roomId]
  }

  getLatestMessages () {
    return this._latestMessages
  }

  getMessages () {
    return this._messages
  }

  //* *******************************************************************************
  // Sync
  //* *******************************************************************************

  finishSync () {
    for (const key in this._syncList) {
      delete this._syncList[key]
    }
    this._syncList = {}
    this._updateLatestMessages()
  }

  setSyncing (isSyncing) {
    this._isSyncing = isSyncing
  }

  pushNextSyncList () {
    _.merge(this._syncList, this._nextSyncList)
    this._nextSyncList = {}
  }

  async getEmptyAllActions () {
    const actions = []

    try {
      const messages = await this._messages.query().fetch()
      for (const message of messages) {
        actions.push(message.prepareDestroyPermanently())
      }

      return actions
    } catch (e) {
      debug('Error getting empty messages actions:', e)
      return actions
    }
  }

  async getSyncActions (collections) {
    debug('Message data to sync:', this._syncList)
    let actions = []

    for (const [roomId, chat] of Object.entries(collections.chats)) {
      if (chat.leave) {
        const messageActions = await this._getMessageRemoveActionsByChat(roomId)
        actions = [...actions, ...messageActions]
      }
    }

    for (const { matrixRoom, messages } of Object.values(this._syncList)) {
      // If we left a room, we deleted all its messages above
      if (collections.chats[matrixRoom.roomId] &&
          collections.chats[matrixRoom.roomId].leave) continue

      // Check if there is a gap in messages
      try {
        if (this._latestMessages[matrixRoom.roomId]) {
          const latestMessage = this._latestMessages[matrixRoom.roomId].getValue()
          if (matrixRoom.getTimelineForEvent(latestMessage.id) !== matrixRoom.getLiveTimeline()) {
            // If the event is not in the live timeline, there is a gap.
            // NOTE: This is true because we only paginate the live timeline backwards,
            // and the live timeline is reset only on gaps.
            // TODO: Implement something more robust
            actions.push(this._messages.prepareCreate(message => {
              message._raw.id = `${matrixRoom.roomId}:${latestMessage.id}`
              message.room.id = matrixRoom.roomId
              message.sender.id = matrix.getUserId()
              message.type = 'missing'
              message.content = { nextEvent: latestMessage.id }
              message.sent = latestMessage.sent
              message.status = 'SENT'
            }))
          }
        }
      } catch (e) {
        debug('Error checking new gap in messages for room %s:', matrixRoom.roomId, e)
      }
      // Remove gaps that are not there anymore
      try {
        const gaps = await this._messages
          .query(Q.where('id', Q.like(`${Q.sanitizeLikeString(matrixRoom.roomId)}%`)))
          .fetch()
        for (const gap in gaps) {
          if (matrixRoom.getTimelineForEvent(gap.content.nextEvent) !== matrixRoom.getLiveTimeline()) {
            // The event is in the live timeline so there is no gap anymore
            actions.push(gap.prepareDestroyPermanently())
          }
        }
      } catch (e) {
        debug('Error checking existing gaps in messages for room %s:', matrixRoom.roomId, e)
      }

      for (const matrixEvent of messages) {
        let mainMatrixEvent = matrixEvent
        // If this event is related to another (and not a reply), work on the main event
        if (matrixEvent.isRelation('m.replace') ||
            matrixEvent.getType() === 'm.reaction' ||
            matrixEvent.isRedaction()) {
          const mainMessageId = matrixEvent.getAssociatedId()
          const matrixEvents = matrixRoom.getLiveTimeline().getEvents()
          mainMatrixEvent = matrixEvents.find(event => event.getId() === mainMessageId)
        }
        // If matrixEvent is null, the message is not in the client,
        // meaning it replaces an event that is too old to appear in the timeline
        if (!mainMatrixEvent) continue

        // Nothing to do if we already handled the main message since we fetched the latest data
        if (!collections.messages[matrixRoom.roomId]) collections.messages[matrixRoom.roomId] = {}
        if (!collections.messages[matrixRoom.roomId][mainMatrixEvent.getId()]) {
          try {
            const message = await this._messages.find(mainMatrixEvent.getId())
            // Update message
            const updatedMessage = this._getMessageUpdateAction(message, mainMatrixEvent, matrixRoom)
            if (updatedMessage) {
              actions.push(updatedMessage)
              collections.messages[matrixRoom.roomId][mainMatrixEvent.getId()] = updatedMessage
            }
          } catch (e) {
            const message = this._getMessageCreateAction(mainMatrixEvent, matrixRoom)
            if (message) {
              actions.push(message)
              collections.messages[matrixRoom.roomId][mainMatrixEvent.getId()] = message
              if (!this._latestMessages[matrixRoom.roomId] ||
                  !this._latestMessages[matrixRoom.roomId].getValue() ||
                  message.sent > this._latestMessages[matrixRoom.roomId].getValue().sent) {
                this._queueLatestMessage(matrixRoom.roomId, message)
              }
            }
            // We will check if we need to create the sender later in users
            if (!collections.users[matrixEvent.getSender()] ||
                collections.users[matrixEvent.getSender()].action === 'delete') {
              collections.users[matrixEvent.getSender()] = { action: 'create' }
            }
          }
        }
      }
    }
    return actions
  }

  _getTypeAndContent (matrixEvent) {
    const message = { type: null, content: null }
    const content = matrixEvent.getContent()
    if (matrixEvent.getType() === 'm.room.member') {
      const prevContent = matrixEvent.getPrevContent()
      if (prevContent) {
        if (content.membership !== prevContent.membership) {
          message.type = 'member.membership'
          const matrixUser = matrix.getUser(matrixEvent.getStateKey())
          message.content = {
            membership: content.membership,
            user: {
              id: matrixUser.userId,
              name: matrixUser.displayName
            }
          }
        } else if (content.avatar_url !== prevContent.avatar_url) {
          message.type = 'member.avatar'
          message.content = content.avatar_url
        } else if (content.displayname !== prevContent.displayname) {
          message.type = 'member.name'
          message.content = {
            prev: prevContent.displayname,
            new: content.displayname
          }
        }
      } else {
        message.type = 'member.membership'
        if (Object.keys(content).length === 0) message.content = null
        else message.content = content
      }
    } else {
      message.type = this._getLocalType(matrixEvent)
      if (Object.keys(content).length === 0) message.content = null
      else message.content = content
    }
    return message
  }

  _getLocalType (matrixEvent) {
    const mainType = matrixEvent.getType()
    let matrixType = mainType
    if (mainType === 'm.room.message') {
      matrixType = matrixEvent.getContent().msgtype
    }

    switch (matrixType) {
      case 'm.room.create':
        return 'room.creation'
      case 'm.room.name':
        return 'room.name'
      case 'm.room.avatar':
        return 'room.avatar'
      case 'm.room.topic':
        return 'room.description'
      case 'm.room.encryption':
        return 'room.encryption'
      case 'm.room.encrypted':
        return 'encrypted'
      case 'm.room.guest_access':
        return 'room.guestAccess'
      case 'm.room.history_visibility':
        return 'room.historyVisibility'
      case 'm.room.join_rules':
        return 'room.joinRules'
      case 'm.room.power_levels':
        return 'room.powerLevels'
      case 'm.text':
        return 'text'
      case 'm.emote':
        return 'emote'
      case 'm.notice':
        return 'notice'
      case 'm.image':
        return 'image'
      case 'm.file':
        return 'file'
      case 'm.audio':
        return 'audio'
      case 'm.location':
        return 'location'
      case 'm.video':
        return 'video'
      default:
        debug('Unknown matrix type:', matrixType)
        return matrixType
    }
  }

  async _addMessage (matrixEvent, status) {
    try {
      debug('Adding single message to db:', matrixEvent)
      const message = await this._messages.database.action(async () => {
        return this._messages.create(message => {
          message._raw.id = matrixEvent.getId()
          message.room.id = matrixEvent.getRoomId()
          message.sender.id = matrixEvent.getSender()
          message.type = this._getLocalType(matrixEvent)
          message.content = matrixEvent.getContent()
          message.sent = matrixEvent.getTs()
          message.status = status
        })
      })
      return message
    } catch (e) {
      debug('Error creating message:', matrixEvent, status)
    }
  }

  _getMessageCreateAction (matrixEvent, matrixRoom) {
    try {
      const newMessage = this._messages.prepareCreate(message => {
        message._raw.id = matrixEvent.getId()
        message.room.id = matrixEvent.getRoomId()
        message.sender.id = matrixEvent.getSender()
        const typeContent = this._getTypeAndContent(matrixEvent)
        message.type = typeContent.type
        message.content = typeContent.content
        message.sent = matrixEvent.getTs()
        message.status = matrixEvent.getAssociatedStatus() || 'SENT'
        const replacingEvent = matrixEvent.replacingEvent()
        if (replacingEvent) {
          message.edited = true
          message.latestUpdate = {
            eventId: replacingEvent.getId(),
            timestamp: replacingEvent.getTs()
          }
        }
        message.reactions = matrix.getMessageReactions(matrixEvent.getId(), matrixRoom)
      })
      return newMessage
    } catch (e) {
      debug('Error creating message %s:', matrixEvent.getId(), e)
      return null
    }
  }

  async _getMessageRemoveActionsByChat (roomId) {
    const actions = []
    try {
      const messages = await this._messages.query(Q.where('room_id', roomId)).fetch()
      for (const message of messages) {
        actions.push(message.prepareDestroyPermanently())
      }

      return actions
    } catch (e) {
      debug('Error removing messages for chat %s:', roomId, e)
      return actions
    }
  }

  _getMessageUpdateAction (message, matrixEvent, matrixRoom) {
    try {
      // If the message was redacted, that's all that matters, otherwise, see what changed
      if (matrixEvent.isRedacted()) {
        if (message.type === 'redacted') return null
        const updatedMessage = message.prepareUpdate(message => {
          message.type = 'redacted'
          message.status = 'SENT'
          const replacingEvent = matrixEvent.replacingEvent()
          if (replacingEvent) {
            message.edited = true
            message.latestUpdate = {
              eventId: replacingEvent.getId(),
              timestamp: replacingEvent.getTs()
            }
          }
          message.reactions = null
        })
        return updatedMessage
      } else {
        // We have two possible changes: m.replace and m.reaction, handle both
        // at the same time
        const messageChanges = []
        const replacingEvent = matrixEvent.replacingEvent()
        if (replacingEvent && replacingEvent.getId() !== message.latestUpdate.eventId) {
          const typeContent = this._getTypeAndContent(matrixEvent)
          messageChanges.push({
            type: 'edited',
            value: {
              type: typeContent.type,
              content: typeContent.content,
              status: replacingEvent.getAssociatedStatus() || 'SENT',
              latestUpdate: {
                eventId: replacingEvent.getId(),
                timestamp: replacingEvent.getTs()
              }
            }
          })
        }
        const reactions = matrix.getMessageReactions(matrixEvent.getId(), matrixRoom)
        if (!_.isEqual(message.reactions, reactions)) {
          messageChanges.push({
            type: 'reactions',
            value: reactions
          })
        }
        if (messageChanges.length > 0) {
          const updatedMessage = message.prepareUpdate(message => {
            for (const change of messageChanges) {
              if (change.type === 'edited') {
                message.type = change.value.type
                message.content = change.value.content
                message.status = change.value.status
                message.edited = true
                message.latestUpdate = change.value.latestUpdate
              } else if (change.type === 'reactions') {
                message.reactions = change.value
              }
            }
          })
          return updatedMessage
        }
        return null
      }
    } catch (e) {
      debug('Error updating message %s with event %s:', message.id, matrixEvent.getId(), e)
      return null
    }
  }

  async _getMessageRemoveAction (messageId) {
    try {
      const message = await this._messages.find(messageId)
      const removedMessage = message.prepareDestroyPermanently()
      return removedMessage
    } catch (e) {
      debug('Error deleting message %s:', messageId, e)
      return null
    }
  }

  async _handleLocalEvent (matrixEvent, matrixRoom, oldEventId) {
    // TODO: handle message not sent
    // Local event IDs are built like this: ~roomid:transaction_id
    if (matrixEvent.getId().startsWith('~' + matrixRoom.roomId)) {
      // It's a local event
      const message = await this._addMessage(matrixEvent, 'SENDING')
      this._queueLatestMessage(matrixRoom.roomId, message)
    } else {
      // It's an event replacing the local event.
      // We receive two of those, so only handle the one with the transaction id
      if (oldEventId) {
        try {
          const newMessage = this._getMessageCreateAction(matrixEvent, matrixRoom)
          const oldMessage = await this._getMessageRemoveAction(oldEventId)
          if (newMessage && oldMessage) {
            debug('Replacing message in database', { oldMessage, newMessage })
            const db = this._messages.database
            await db.action(async (action) => db.batch(newMessage, oldMessage))

            this._queueLatestMessage(matrixRoom.roomId, newMessage)
          } else {
            throw Error('No new message or old message')
          }
        } catch (e) {
          debug('Error replacing local message %s with message %s:',
            oldEventId, matrixEvent.getId(), e)
        }
      }
    }
  }

  _handleTimelineEvent (matrixEvent, matrixRoom) {
    // If it's a local event, it will be handled separately
    if (matrixEvent.getId().startsWith('~' + matrixRoom.roomId)) return
    const syncList = this._isSyncing ? this._nextSyncList : this._syncList
    if (!syncList[matrixRoom.roomId]) {
      syncList[matrixRoom.roomId] = {
        matrixRoom,
        messages: []
      }
    }

    switch (matrixEvent.getType()) {
      case 'm.reaction':
      case 'm.room.avatar':
      case 'm.room.create':
      case 'm.room.encrypted':
      case 'm.room.encryption':
      case 'm.room.guest_access':
      case 'm.room.history_visibility':
      case 'm.room.join_rules':
      case 'm.room.member':
      case 'm.room.message':
      case 'm.room.name':
      case 'm.room.power_levels':
      case 'm.room.redaction':
      case 'm.room.topic':
        syncList[matrixRoom.roomId].messages.push(matrixEvent)
        this._isSyncing ? this._nextSyncList = syncList : this._syncList = syncList
        break
      default:
        debug('Unhandled message event of type %s received:', matrixEvent.getType(), matrixEvent)
    }
  }

  _updateLatestMessages () {
    for (const roomId in this._nextLatestMessages) {
      if (!this._latestMessages[roomId]) this._latestMessages[roomId] = new BehaviorSubject(null)
      this._latestMessages[roomId].next(this._nextLatestMessages[roomId])
    }
    this._nextLatestMessages = {}
  }

  _queueLatestMessage (roomId, message) {
    if (!this._nextLatestMessages[roomId] ||
        this._nextLatestMessages[roomId].sent < message.sent) {
      this._nextLatestMessages[roomId] = message
    }
  }
}

const messageService = new MessageService()
export default messageService
