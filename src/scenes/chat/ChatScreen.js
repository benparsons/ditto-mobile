import withObservables from '@nozbe/with-observables'
import { Icon, TopNavigation, useTheme } from '@ui-kitten/components'
import React from 'react'
import { KeyboardAvoidingView, SafeAreaView, View } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import Touchable from 'react-native-platform-touchable'

import { isIos } from '../../utilities/Misc'
import chats from './chats'
import Composer from './components/Composer'
import Timeline from './components/Timeline'
import messages from './message/messages'

const debug = require('debug')('ditto:scenes:chat:ChatScreen')

function ChatScreen ({ navigation, route, chat }) {
  const theme = useTheme()

  //* *******************************************************************************
  // Methods
  //* *******************************************************************************
  const onSend = messageText => {
    messages.send(messageText, 'text', chat.id)
  }

  const handleImagePick = () => {
    try {
      ImagePicker.showImagePicker(async response => {
        if (response.didCancel) return

        messages.send(response, 'image', chat.id)
      })
    } catch (e) {
      debug('onImagePick error', e)
    }
  }

  const renderBackButton = () => (
    <Touchable onPress={() => navigation.goBack()}>
      <Icon name={`arrow-${isIos() ? 'ios-' : ''}back`} width={35} height={35} fill={theme['text-basic-color']} />
    </Touchable>
  )

  //* *******************************************************************************
  // Lifecycle
  //* *******************************************************************************

  return (
    <>
      <SafeAreaView style={{ backgroundColor: theme['background-basic-color-2'] }}>
        <TopNavigation
          title={chat.name || route.params.roomTitle || '...'}
          alignment='center'
          leftControl={renderBackButton()}
          style={{ backgroundColor: theme['background-basic-color-2'] }}
        />
      </SafeAreaView>
      <Wrapper style={{ flex: 1, backgroundColor: theme['background-basic-color-4'] }}>
        <Timeline chat={chat} />
        <Composer
          chat={chat}
          onSend={onSend}
          onImagePick={handleImagePick}
        />
      </Wrapper>
    </>
  )
}
const enhance = withObservables(['route'], ({ route }) => ({
  chat: chats.getChat(route.params.roomId)
}))
export default enhance(ChatScreen)

const Wrapper = ({ style, children }) => {
  return isIos() ? (
    <KeyboardAvoidingView style={style} behavior='padding'>
      {children}
    </KeyboardAvoidingView>
  ) : (
    <View style={style}>{children}</View>
  )
}
