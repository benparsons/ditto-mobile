import authService from './service'

// const debug = require('debug')('ditto:scenes:auth:auth')

class Auth {
  getError () {
    return authService.getError()
  }

  resetError () {
    authService.setAction({
      type: authService.actions.ERROR,
      payload: null
    })
  }

  getUserId () {
    return authService.getData().getValue().userId
  }

  isLoaded () {
    return authService.isLoaded()
  }

  isLoggedIn () {
    return authService.isLoggedIn()
  }

  login (username, password, homeserver) {
    authService.setAction({
      type: authService.actions.LOGIN,
      payload: {
        username,
        password,
        homeserver
      }
    })
  }

  logout () {
    authService.setAction({ type: authService.actions.LOGOUT })
  }
}

const auth = new Auth()
export default auth
