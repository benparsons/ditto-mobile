import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'

import LandingScreen from './LandingScreen'
import LoginScreen from './LoginScreen'

const Stack = createStackNavigator()

export default function AuthScreens () {
  return (
    <Stack.Navigator headerMode='none'>
      <Stack.Screen name='Landing' component={LandingScreen} />
      <Stack.Screen name='Login' component={LoginScreen} />
    </Stack.Navigator>
  )
}
