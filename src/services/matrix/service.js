import { EventTimeline } from 'matrix-js-sdk'
import { getDeviceName } from 'react-native-device-info'
import {
  MATRIX_PUSH_GATEWAY_APNS,
  MATRIX_PUSH_GATEWAY_FCM,
  MATRIX_PUSH_GATEWAY_URL
} from 'react-native-dotenv'
import { BehaviorSubject, Subject } from 'rxjs'
import showdown from 'showdown'

import auth from '../../scenes/auth/service'
import { isIos, toImageBuffer } from '../../utilities/Misc'
import matrixClient from './client'

const debug = require('debug')('ditto:services:matrix:service')
const mdConverter = new showdown.Converter()

// TODO: Catch M_UNKNOWN_TOKEN exception on Session.LoggedOut

class MatrixService {
  _ready
  _action
  actions

  constructor () {
    this._ready = new BehaviorSubject(false)
    this._action = new Subject(null)
    this.actions = {
      CLIENT_ERROR: 'client/error',
      CLIENT_LOGGED_OUT: 'client/loggedOut',
      CLIENT_SYNCING: 'client/syncing',
      EVENT_ACCOUNT_SETTINGS: 'event/account/settings',
      EVENT_ROOM_ACCOUNT: 'event/room/account',
      EVENT_ROOM_DELETE: 'event/room/delete',
      EVENT_ROOM_LOCAL: 'event/room/local',
      EVENT_ROOM_MEMBER: 'event/room/member',
      EVENT_ROOM_RECEIPT: 'event/room/receipt',
      EVENT_ROOM_STATE: 'event/room/state',
      EVENT_ROOM_TIMELINE: 'event/room/timeline',
      EVENT_ROOM_TYPING: 'event/room/typing',
      EVENT_UNKNOWN: 'event/unknown',
      EVENT_USER: 'event/user',
      MESSAGE_SEND: 'message/send',
      ROOM_START_REACHED: 'room/startReached'
    }
    this._room = null
  }

  init () {
    // Auth
    auth.getAction().subscribe((action) => {
      const payload = action.payload
      switch (action.type) {
        case auth.actions.LOGIN:
          this._login(payload.username, payload.password, payload.homeserver)
          break
        case auth.actions.LOGOUT:
          this._logout()
          break
        default:
      }
    })
    auth.isLoggedIn().subscribe((authLoggedIn) => {
      if (authLoggedIn) {
        this._start()
      }
    })

    // Matrix
    this.getAction().subscribe((action) => {
      switch (action.type) {
        case this.actions.CLIENT_LOGGED_OUT:
          auth.setAction({ type: auth.actions.FORCED_LOGOUT })
          break
        default:
      }
    })
  }

  isReady () {
    return this._ready
  }

  getAction () {
    return this._action
  }

  setAction (action) {
    if (!Object.values(this.actions).includes(action.type)) throw Error('Unknown action type:', action)
    this._action.next(action)
  }

  async createRoom (options) {
    try {
      const response = await matrixClient.get().createRoom(options)
      return this.getRoom(response.room_id)
    } catch (e) {
      debug('Error creating room:', e)
      return null
    }
  }

  async fetchPreviousMessages (roomId) {
    if (!matrixClient.get()) {
      debug('fetchPreviousMessages: no client. We are probably logged out.')
      return null
    }

    const room = matrixClient.get().getRoom(roomId)
    // NOTE: This is limited since it paginates the live timeline, not our own
    // messages timeline. Meaning we might need to paginate several times before
    // really getting new messages…
    // TODO: Improve this and gaps detection
    await matrixClient.get().paginateEventTimeline(room.getLiveTimeline(), { backwards: true })
    const start = !room.getLiveTimeline().getPaginationToken(EventTimeline.BACKWARDS)
    if (start) this.setAction({ type: this.actions.ROOM_START_REACHED, payload: { room } })
    this.setAction({ type: this.actions.CLIENT_SYNCING })
  }

  async getEvent (roomId, eventId) {
    if (!matrixClient.get()) {
      debug('getEvent: no client. We are probably logged out.')
      return null
    }
    const timelineSet = matrixClient.get().getRoom(roomId).getUnfilteredTimelineSet()
    await matrixClient.get().getEventTimeline(timelineSet, eventId)
    return timelineSet.findEventById(eventId)
  }

  getImageUrl (mxcUrl, width, height, resizeMethod = 'scale') {
    if (!matrixClient.get()) {
      debug('getImageUrl: no client. We are probably logged out.')
      return null
    }

    if (width == null && height == null) {
      return matrixClient.get().mxcUrlToHttp(mxcUrl)
    } else {
      return matrixClient.get().mxcUrlToHttp(mxcUrl, width, height, resizeMethod)
    }
  }

  getMessageReactions (eventId, room) {
    const eventReactions = room.getUnfilteredTimelineSet().getRelationsForEvent(eventId, 'm.annotation', 'm.reaction')
    const sortedReactions = eventReactions?._sortedAnnotationsByKey
    if (sortedReactions && sortedReactions.length > 0) {
      const reactions = {}
      for (const reaction of sortedReactions) {
        const key = reaction[0]
        const users = Array.from(reaction[1])
        reactions[key] = users
      }
      return reactions
    }
    return null
  }

  getRoom (roomId) {
    if (!matrixClient.get()) {
      debug('getRoom: no client. We are probably logged out.')
      return null
    }

    return matrixClient.get().getRoom(roomId)
  }

  getRoomAvatar (room) {
    const roomState = room.getLiveTimeline().getState(EventTimeline.FORWARDS)
    const avatarEvent = roomState.getStateEvents('m.room.avatar', '')
    let avatar = avatarEvent ? avatarEvent.getContent().url : null

    if (!avatar) {
      if (this.isRoomDirect(room.roomId)) avatar = room.getAvatarFallbackMember()?.user?.avatarUrl
    }
    return avatar
  }

  getRoomDescription (roomState) {
    const descEvent = roomState.getStateEvents('m.room.topic', '')
    const description = descEvent ? descEvent.getContent().topic : null
    return description
  }

  getRoomFullyRead (room) {
    const fullyReadEvent = room.getAccountData('m.fully_read')
    const fullyRead = fullyReadEvent ? fullyReadEvent.getContent().event_id : null
    return fullyRead
  }

  getUser (userId) {
    if (!matrixClient.get()) {
      debug('getUser: no client. We are probably logged out.')
      return null
    }

    return matrixClient.get().getUser(userId)
  }

  getUserId () {
    if (!matrixClient.get()) {
      debug('getUserId: no client. We are probably logged out.')
      return null
    }

    return matrixClient.get().getUserId()
  }

  getUserReadReceiptInRoom (room, userId) {
    const receipts = room._receipts
    const readReceipts = receipts ? receipts['m.read'] : null
    const readReceipt = readReceipts ? readReceipts[userId] : null
    return readReceipt
  }

  async hasPusher (token) {
    const { pushers } = await matrixClient.get().getPushers()

    let pushkey = token
    let appId = null
    if (isIos()) {
      appId = MATRIX_PUSH_GATEWAY_APNS
      pushkey = Buffer.from(token, 'hex').toString('base64')
    } else {
      appId = MATRIX_PUSH_GATEWAY_FCM
    }

    for (const pusher of pushers) {
      if (pusher.app_id === appId && pusher.pushkey === pushkey) {
        return true
      }
    }
    return false
  }

  isRoomDirect (roomId) {
    if (!matrixClient.get()) {
      debug('isRoomDirect: no client. We are probably logged out.')
      return null
    }

    const directEvent = matrixClient.get().getAccountData('m.direct')
    const directRooms = directEvent ? Object.keys(directEvent.getContent()) : []
    if (directRooms.includes(roomId)) return true

    const room = matrixClient.get().getRoom(roomId)
    if (room.getJoinedMemberCount() <= 2) return true
    return false
  }

  async leaveRoom (roomId) {
    if (!matrixClient.get()) {
      debug('getImageUrl: no client. We are probably logged out.')
      return null
    }

    try {
      await matrixClient.get().leave(roomId)
      await matrixClient.get().forget(roomId)
    } catch (e) {
      debug('Error leaving room %s:', roomId, e)
    }
  }

  async searchUsers (searchText) {
    return matrixClient.get().searchUserDirectory({ term: searchText })
  }

  async sendMessage (content, type, roomId) {
    if (!matrixClient.get()) {
      debug('sendMessage: no client. We are probably logged out.')
      return null
    }
    // Send message
    try {
      switch (type) {
        case 'text': {
          const html = mdConverter.makeHtml(content)
          debug('Message html:', html)
          // If the message doesn't have markdown, don't send the html
          if (html !== '<p>' + content + '</p>') {
            return matrixClient.get().sendHtmlMessage(roomId, content, html)
          } else {
            return matrixClient.get().sendTextMessage(roomId, content)
          }
        }
        case 'image': {
          const imageUrl = await this.uploadImage(content)
          if (!imageUrl) return null

          await matrixClient.get().sendImageMessage(
            roomId,
            imageUrl,
            {
              w: content.width,
              h: content.height,
              mimetype: content.type,
              size: content.fileSize
            },
            content.fileName
          )
          break
        }
        default:
          debug('Unhandled message type to send %s:', type, content)
          return
      }
    } catch (e) {
      debug('Error sending message:', { roomId, type, content }, e)
    }
  }

  async sendReadReceipt (roomId, eventId) {
    if (!matrixClient.get()) {
      debug('sendReadReceipt: no client. We are probably logged out.')
      return null
    }
    try {
      const room = matrixClient.get().getRoom(roomId)
      const evt = room?.getUnfilteredTimelineSet().findEventById(eventId)
      await matrixClient.get().sendReadReceipt(evt)
    } catch (e) {
      debug('Error sending read receipt for event %s', eventId, e)
    }
  }

  async sendTyping (roomId, isTyping, timeout) {
    if (!matrixClient.get()) {
      debug('sendTyping: no client. We are probably logged out.')
      return null
    }
    try {
      await matrixClient.get().sendTyping(roomId, isTyping, timeout)
    } catch (e) {
      debug('Error sending typing event for room %s', roomId, e)
    }
  }

  async setAvatar (url) {
    await matrixClient.setAvatarUrl(url)
  }

  async setDisplayName (name) {
    await matrixClient.setDisplayName(name)
  }

  async setPusher (token) {
    if (!matrixClient.get()) {
      debug('setPusher: no client. We are probably logged out.')
      return null
    }

    const deviceName = await getDeviceName() // TODO: Set on login and get from matrix client?

    let appId = null
    let pushkey = token
    if (isIos()) {
      appId = MATRIX_PUSH_GATEWAY_APNS
      // Convert to base64 since that's what sygnal expects
      pushkey = Buffer.from(token, 'hex').toString('base64')
    } else {
      appId = MATRIX_PUSH_GATEWAY_FCM
    }

    const pusher = {
      lang: 'en', // TODO:i18n: Get from device?
      kind: 'http',
      app_display_name: 'Ditto', // TODO: Get from app?
      device_display_name: deviceName,
      app_id: appId,
      pushkey,
      data: {
        url: MATRIX_PUSH_GATEWAY_URL,
        format: 'event_id_only'
      },
      append: false
    }

    debug('Setting Pusher:', pusher)
    try {
      const response = await matrixClient.get().setPusher(pusher)
      debug('Pusher response: ', response)
      if (Object.keys(response).length > 0) {
        debug('Error registering pusher on matrix homeserver:', response)
      }
    } catch (e) {
      debug('Error registering pusher on matrix homeserver:', e)
    }
  }

  async uploadImage (image) {
    if (!matrixClient.get()) {
      debug('isRoomDirect: no client. We are probably logged out.')
      return null
    }
    try {
      const url = await matrixClient.get().uploadContent(toImageBuffer(image.data), {
        onlyContentUri: true,
        name: image.fileName,
        type: image.type
      })
      return url
    } catch (e) {
      debug('Error uploading image:', e)
      return null
    }
  }

  _attachListeners () {
    if (!matrixClient.get()) throw Error('No client to add listeners to')
    matrixClient.get().on('event', this._onMatrixEvent.bind(this))
    matrixClient.get().on('accountData', this._onAccountDataEvent.bind(this))
    matrixClient.get().on('deleteRoom', (roomId) => {
      this.setAction({ type: this.actions.EVENT_ROOM_DELETE, payload: { roomId } })
    })
    matrixClient.get().on('Room.accountData', (event, room) => {
      this.setAction({ type: this.actions.EVENT_ROOM_ACCOUNT, payload: { event, room } })
    })
    matrixClient.get().on('Room.localEchoUpdated', (event, room, oldEventId, oldStatus) => {
      this.setAction({
        type: this.actions.EVENT_ROOM_LOCAL,
        payload: { event, room, oldEventId, oldStatus }
      })
    })
    matrixClient.get().on('Room.receipt', (event, room) => {
      this.setAction({ type: this.actions.EVENT_ROOM_RECEIPT, payload: { event, room } })
    })
    matrixClient.get().on('Room.timeline', (event, room) => {
      this.setAction({ type: this.actions.EVENT_ROOM_TIMELINE, payload: { event, room } })
    })
    matrixClient.get().on('RoomMember.membership', (event, member) => {
      this.setAction({ type: this.actions.EVENT_ROOM_MEMBER, payload: { event, member } })
    })
    matrixClient.get().on('RoomMember.powerLevel', (event, member) => {
      this.setAction({ type: this.actions.EVENT_ROOM_MEMBER, payload: { event, member } })
    })
    matrixClient.get().on('RoomMember.typing', (event, member) => {
      this.setAction({ type: this.actions.EVENT_ROOM_TYPING, payload: { event, member } })
    })
    matrixClient.get().on('RoomState.events', (event, roomState) => {
      this.setAction({ type: this.actions.EVENT_ROOM_STATE, payload: { event, roomState } })
    })
    matrixClient.get().on('Session.logged_out', () => {
      this.setAction({ type: this.actions.CLIENT_LOGGED_OUT, payload: null })
    })
    matrixClient.get().on('User.lastPresenceTs', (event, user) => {
      this.setAction({ type: this.actions.EVENT_USER, payload: { event, user } })
    })
  }

  async _login (username, password, homeserver) {
    const response = await matrixClient.login(username, password, homeserver)
    if (response.error) {
      auth.setAction({
        type: auth.actions.ERROR,
        payload: response.data
      })
    } else {
      auth.setAction({
        type: auth.actions.LOGIN_RESPONSE,
        payload: response.data
      })
    }
  }

  _logout () {
    matrixClient.logout()
    this._ready.next(false)
  }

  _onMatrixEvent (event) {
    switch (event.getType()) {
      // Those events are already handled
      case 'm.fully_read':
      case 'm.presence':
      case 'm.receipt':
      case 'm.room.avatar':
      case 'm.room.create':
      case 'm.room.guest_access':
      case 'm.room.history_visibility':
      case 'm.room.join_rules':
      case 'm.room.member':
      case 'm.room.message':
      case 'm.room.name':
      case 'm.room.power_levels':
      case 'm.room.redaction':
      case 'm.room.topic':
      case 'm.typing':
      case 'm.reaction':
        break
      default:
        debug('Unhandled matrix event received:', event)
        this.setAction({ type: this.actions.EVENT_UNKNOWN, payload: { event } })
    }
  }

  _onAccountDataEvent (event) {
    switch (event.getType()) {
      case 'm.direct':
        // According to the spec, it is only set on invite/room creation, so we
        // will only look at that time
        break
      case 'm.push_rules':
        this.setAction({ type: this.actions.EVENT_ACCOUNT_SETTINGS, payload: { event } })
        break
      default:
    }
  }

  _onSyncEvent (event) {
    switch (event) {
      case 'PREPARED':
        this._ready.next(true)
        break
      case 'SYNCING':
        this.setAction({ type: this.actions.CLIENT_SYNCING })
        break
      case 'ERROR':
        debug('A syncing error ocurred:', event)
        this.setAction({
          type: this.actions.CLIENT_ERROR,
          payload: event
        })
        break
      default:
    }
  }

  _start () {
    const { userId, accessToken, homeserver } = auth.getData().getValue()
    if (this._ready.getValue() || !userId || !accessToken || !homeserver) return

    if (!matrixClient.get()) {
      matrixClient.create(userId, accessToken, homeserver)
    }

    matrixClient.get().on('sync', this._onSyncEvent.bind(this))
    this._attachListeners()

    try {
      matrixClient.start()
    } catch (e) {
      if (e.errCode === 'M_UNKNOWN_TOKEN') debug('Unknown token. We should log out.')
      else throw e
    }
  }
}

const matrix = new MatrixService()
export default matrix
