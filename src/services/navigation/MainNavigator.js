import withObservables from '@nozbe/with-observables'
import { createStackNavigator } from '@react-navigation/stack'
import { Avatar, Icon, Text, TopNavigation, useTheme } from '@ui-kitten/components'
import React from 'react'
import { SafeAreaView } from 'react-native'
import Touchable from 'react-native-platform-touchable'
import { of as of$ } from 'rxjs'
import { switchMap } from 'rxjs/operators'

import auth from '../../scenes/auth/auth'
import chats from '../../scenes/chat/chats'
import ChatScreen from '../../scenes/chat/ChatScreen'
import HomeScreens from '../../scenes/home/HomeScreens'
import NewChatScreens from '../../scenes/newChat/NewChatScreens'
import SettingsScreen from '../../scenes/settings/SettingsScreen'
import users from '../../scenes/user/users'
import { isIos } from '../../utilities/Misc'

// const debug = require('debug')('ditto:services:navigation:MainNavigator')

const Stack = createStackNavigator()

function MainScreens () {
  const myUserId = auth.getUserId()
  return (
    <Stack.Navigator headerMode='screen'>
      <Stack.Screen
        name='Home'
        options={({ navigation: nav }) => ({ header: () => <HomeHeader nav={nav} userId={myUserId} /> })}
        component={HomeScreens}
      />
      <Stack.Screen
        name='Chat'
        options={{ headerShown: false }}
        component={ChatScreen}
      />
    </Stack.Navigator>
  )
}

export default function MainNavigator () {
  return (
    <Stack.Navigator mode='modal' headerMode='none'>
      <Stack.Screen name='Main' component={MainScreens} />
      <Stack.Screen name='Settings' component={SettingsScreen} />
      <Stack.Screen name='NewChat' component={NewChatScreens} />
    </Stack.Navigator>
  )
}

const HeaderLeft = ({ userId, user, nav }) => {
  return (
    <Touchable onPress={() => nav.navigate('Settings')} style={{ flexDirection: 'row', alignItems: 'center' }}>
      <>
        <Avatar
          source={{ uri: user ? chats.getAvatarUrl(user.avatar, 45) : null }}
          style={{ marginLeft: 6, marginRight: 12, width: 45, height: 45 }}
          defaultSource={require('../../assets/images/placeholder.png')}
        />
        <Text category='h6'>{user ? user.name : userId}</Text>
      </>
    </Touchable>
  )
}
const enhanceHeader = withObservables(['userId', 'users'], ({ userId }) => ({
  user: users.getCount().pipe(switchMap(count => count > 0 ? users.getUser(userId) : of$(null)))
}))
const EnhancedHeaderLeft = enhanceHeader(HeaderLeft)

const renderHeaderRight = (theme, nav) => {
  return isIos() ? (
    <Touchable
      onPress={() => nav.navigate('NewChat')}
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: theme['color-primary-active'],
        padding: 4,
        borderRadius: 50,
        marginRight: 6
      }}
    >
      <Icon name='plus' width={20} height={20} fill={theme['text-basic-color']} />
    </Touchable>
  ) : null
}

const HomeHeader = ({ nav, userId }) => {
  const theme = useTheme()
  return (
    <SafeAreaView style={{ backgroundColor: theme['background-basic-color-2'] }}>
      <TopNavigation
        style={{ backgroundColor: theme['background-basic-color-2'] }}
        leftControl={(<EnhancedHeaderLeft nav={nav} userId={userId} />)}
        rightControls={renderHeaderRight(theme, nav)}
        alignment='start'
      />
    </SafeAreaView>
  )
}
