---
name: [Ditto] Add features to this mobile client!
desc: This project will focus on implementing a variety of smaller features to the Matrix client Ditto.
# add a short one line description of your project
requirements:
  # Student requirements:
  - Familiarity (even a little) with React Native
difficulty: low
issues:
  # Related issues (if any)  to this project.
  - https://gitlab.com/ditto-chat/ditto-mobile/issues/31
  - https://gitlab.com/ditto-chat/ditto-mobile/issues/27
  - https://gitlab.com/ditto-chat/ditto-mobile/issues/23
mentors:
  # First person in contact; mentors may change before project starts.
  - Annie Elequin
initiatives:
  - GSOC
tags:
  # Different technologies needed
  - JavaScript (React Native)
---

#### Description

This project will focus on implementing a variety of smaller features to the Matrix client Ditto.
Ditto is a mobile client built with React Native, and it's fairly new, so there are a lot of features
that need to be implemented to bring it to feature parity with typical chat apps. Each of these issues
will be smaller issues that each add to the user experience.

#### Milestones

##### GSOC CODING STARTS

- Get Ditto running on local machine
- Start learning about the Ditto codebase and Matrix API

##### GSOC MIDTERM

- Link Previews in Chat (#31)
- Save media to phone (#23)

##### GSOC FINAL

- Remove messages (#27)
