/**
 * @format
 */

import { firebase } from '@react-native-firebase/messaging'
import { AppRegistry, NativeModules, Platform } from 'react-native'

import App from './App'
import { name as appName } from './app.json'
import matrix from './src/services/matrix/service'
import notifications from './src/services/notifications/service'
import storage from './src/services/storage/service'

// Init services. Storage should go last since it will trigger everything
notifications.init()
matrix.init()
storage.init()

// fix for https://github.com/kmagiera/react-native-gesture-handler/issues/320
if (Platform.OS === 'android') {
  const { UIManager } = NativeModules
  if (UIManager) {
    // Add gesture specific events to genericDirectEventTypes object exported from UIManager native module.
    // Once new event types are registered with react it is possible to dispatch these events to all kind of native views.
    UIManager.genericDirectEventTypes = {
      ...UIManager.genericDirectEventTypes,
      onGestureHandlerEvent: { registrationName: 'onGestureHandlerEvent' },
      onGestureHandlerStateChange: {
        registrationName: 'onGestureHandlerStateChange'
      }
    }
  }
}

AppRegistry.registerComponent(appName, () => App)

if (Platform.OS === 'android') {
  // Listen for FCM push notifications when app is in background
  firebase.messaging().setBackgroundMessageHandler(async remoteMessage => {
    const debug = require('debug')('ditto:index:BackgroundMessageHandler')
    debug('Received FCM Message while in background:', remoteMessage)

    const data = remoteMessage.data
    if (data.event_id != null && data.room_id != null) {
      notifications.setAction({
        type: notifications.actions.DISPLAY,
        payload: { eventId: data.event_id, roomId: data.room_id }
      })
    } else {
      debug(`Problem with eventId (${data.event_id}) or roomId(${data.room_id})`)
    }
  })
}
